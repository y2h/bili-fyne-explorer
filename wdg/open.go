package wdg

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/storage"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	fxl "fyne.io/x/fyne/layout"
)

func NewOpenFile1(data binding.String, filters []string,
	fn func(), w fyne.Window) *fyne.Container {
	en0 := widget.NewEntryWithData(data)
	en0.PlaceHolder = "Choose a file"
	btn1 := widget.NewButtonWithIcon("", theme.FolderOpenIcon(), func() {
		d := dialog.NewFileOpen(func(uc fyne.URIReadCloser, e error) {
			if uc == nil {
				return
			}
			data.Set(uc.URI().Path())
			if fn != nil {
				fn()
			}
		}, w)
		luri, _ := storage.ListerForURI(storage.NewFileURI("."))
		d.SetFilter(storage.NewExtensionFileFilter(filters))
		d.SetLocation(luri)
		d.Show()
	})
	return container.New(&fxl.HPortion{Portions: []float64{10, 90}}, btn1, en0)
}
