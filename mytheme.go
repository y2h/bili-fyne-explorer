package main

import (
	_ "embed"
	"image/color"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/theme"
)

// 得意黑https://github.com/atelier-anchor/smiley-sans
//
//go:embed SmileySans-Oblique.ttf
var font []byte

var myfont = &fyne.StaticResource{
	StaticName:    "FZLTCXHJW",
	StaticContent: font,
}

type theme1 struct{}

var _ fyne.Theme = (*theme1)(nil)

func (*theme1) Font(s fyne.TextStyle) fyne.Resource {
	// return theme.DefaultEmojiFont()
	return myfont
}

func (*theme1) Color(n fyne.ThemeColorName, v fyne.ThemeVariant) color.Color {
	return theme.DefaultTheme().Color(n, v)
}

func (*theme1) Icon(n fyne.ThemeIconName) fyne.Resource {
	return theme.DefaultTheme().Icon(n)
}

func (*theme1) Size(n fyne.ThemeSizeName) float32 {
	// if n==theme.SizeNameInputRadius{
	// 	return 40
	// }
	// if n==theme.SizeNamePadding{
	// 	return 0
	// }
	return theme.DefaultTheme().Size(n) * 1.0
}
