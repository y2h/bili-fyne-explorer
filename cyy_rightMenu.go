package main

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/storage"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitee.com/y2h/fyneExplorer/me"
	"github.com/gogf/gf/v2/os/gfile"
)

func (f *fileDialog) menuRight() {
	me.MenuRight = widget.NewPopUpMenu(fyne.NewMenu("",
		fyne.NewMenuItem("View", func() {
			me.ViewFile(f.selected.Path(), f.dir)
		}),
		fyne.NewMenuItem("Open", func() {
			f.OpenWithDefaultApp(func(err error) {
				me.Msg(err.Error())
			})
		}),
		fyne.NewMenuItem("复制路径", func() {
			f.CpPath()
			me.PathToPasted = f.selected.Path()
			me.BtnPaste.Enable()
		}),
		fyne.NewMenuItem("Rename", func() {
			f.Rename()
		}),
		fyne.NewMenuItem("Delete", func() {
			f.Delete()
		}),
		fyne.NewMenuItem("添加到收藏夹", func() {
			f.AddFavorite()
		}),
	), f.w.Canvas())
}

func (f *fileDialog) CpPath() {
	if f.selected == nil || f.selected.Path() == "" {
		me.Msg("没有选择项目")
		return
	}
	f.w.Clipboard().SetContent(f.selected.Path())
	me.Msg("路径已复制到剪贴板:" + f.selected.Path())
}

func (f *fileDialog) OpenWithDefaultApp(errfn func(err error)) {
	path := f.selected.Path()
	if f.selected == nil || path == "" {
		me.Msg("请选择需要打开的项")
		return
	}
	if gfile.Exists(path) {
		var err error
		switch runtime.GOOS {
		case "linux":
			err = exec.Command("xdg-open", path).Start()
		case "windows":
			err = exec.Command("rundll32", "url.dll,FileProtocolHandler", path).Start()
		case "darwin":
			err = exec.Command("open", path).Start()
		default:
			err = fmt.Errorf("unsupported platform")
		}
		if err != nil && errfn != nil {
			errfn(err)
		}
	}
}

func (f *fileDialog) Rename() {
	if f.selected == nil || gfile.Name(f.selected.Path()) == "(Parent)" {
		return
	}
	newNameEntry := widget.NewEntry()
	newNameEntry.FocusGained()
	newNameEntry.SetText(f.selected.Name())
	dialog.ShowForm("Rename", "Rename", "Cancel", []*widget.FormItem{
		{
			Text:   "Old Name",
			Widget: widget.NewLabel(f.selected.Path()),
		},
		{
			Text:   "New Name",
			Widget: newNameEntry,
		},
	}, func(s bool) {
		if !s || newNameEntry.Text == "" {
			return
		}

		newNamePath := filepath.Join(f.dir.Path(), newNameEntry.Text)
		err := os.Rename(f.selected.Path(), newNamePath)
		if err != nil {
			dialog.ShowError(errors.New("Rename err : "+err.Error()), f.w)
		}
		f.refreshDir(f.dir)
		me.Msg("成功重命名")
	}, f.w)
}
func (f *fileDialog) AddFavorite() {
	if !me.IsDir(f.selected) {
		me.Msg("请选择目录")
		return
	}

	for _, v := range f.favorites {
		if v.loc.Path() == f.selected.Path() {
			me.Msg("已在收藏夹")
			return
		}
	}
	f.favorites = append(f.favorites, favoriteItem{
		locName: f.selected.Name(),
		locIcon: theme.FolderIcon(),
		loc:     f.selected,
	})
	f.favoritesList.Refresh()
	me.FavoritesAdded = append(me.FavoritesAdded, f.selected.Path())
	me.Msg("成功添加到收藏夹")
}
func (f *fileDialog) Delete() {
	if f.selected != nil && gfile.Name(f.selected.Path()) != "(Parent)" {
		dialog.ShowConfirm("Delete file",
			"Are you sure to delete \n\n[ "+f.selected.Path()+" ]", func(b bool) {
				if b {
					f.files.Unselect(f.selectedID)
					storage.Delete(f.selected)
					f.refreshDir(f.dir)
					// 删除后自动选择下一个
					if f.selectedID < len(f.data)-1 {
						f.files.Select(f.selectedID)
					} else {
						f.files.Select(f.selectedID - 1)
					}
				}
			}, f.w)
	}
}
