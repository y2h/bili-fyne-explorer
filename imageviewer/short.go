// Copyright 2023 ChenHao(bl4cyy: https://space.bilibili.com/313454627 ,
// 腾讯云 https://cloud.tencent.com/developer/user/6167008 ,
// 开源中国 https://my.oschina.net/u/3820046,
// y2h: https://gitee.com/y2h ) . All rights reserved.
// Use of this source code is governed by a
// license that can be found in the LICENSE file.
package imageviewer

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

var vbox = container.NewVBox
var hbox = container.NewHBox
var border = container.NewBorder
var sr = fyne.NewStaticResource
var gridC = container.NewGridWithColumns
var gridR = container.NewGridWithRows
var gridA = container.NewAdaptiveGrid
var gridW = container.NewGridWrap
var hsplit = container.NewHSplit

// var vsplit=container.NewVSplit
var tabs = container.NewAppTabs
var tabi = container.NewTabItem
var lbl = widget.NewLabel
var list = widget.NewList
var lblD = widget.NewLabelWithData
var imgI = canvas.NewImageFromImage
var imgR = canvas.NewImageFromResource
var imgF = canvas.NewImageFromFile
var dataListen = binding.NewDataListener
var btn = widget.NewButton
var btnIcon = widget.NewButtonWithIcon
var entry = widget.NewEntry
var entryData = widget.NewEntryWithData
var slider = widget.NewSlider
var link = widget.NewHyperlink
var scroll = container.NewScroll
var hscroll = container.NewHScroll
var vscroll = container.NewVScroll
var space = layout.NewSpacer
var sel = widget.NewSelect
var check = widget.NewCheck
var radios = widget.NewRadioGroup
var acdItem = widget.NewAccordionItem
