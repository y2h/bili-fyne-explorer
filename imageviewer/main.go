package imageviewer

import (
	"fmt"
	"os"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/widget"
)

type enterEntry struct {
	widget.Entry
	enterFunc func(s string)
}

func (e *enterEntry) TypedKey(key *fyne.KeyEvent) {
	switch key.Name {
	case fyne.KeyReturn:
		e.enterFunc(e.Text)
	default:
		e.Entry.TypedKey(key)
	}
}

func newEnterEntry() *enterEntry {
	entry := &enterEntry{}
	entry.ExtendBaseWidget(entry)
	return entry
}

type editingSlider struct {
	widget.Slider
	dragEndFunc func(float64)
}

func (e *editingSlider) DragEnd() {
	e.dragEndFunc(e.Value)
}

func newEditingSlider(min, max float64) *editingSlider {
	editSlider := &editingSlider{}
	editSlider.Max = max
	editSlider.Min = min
	editSlider.ExtendBaseWidget(editSlider)
	return editSlider
}

// newEditingOption creates a new VBox, that includes an info text and a widget to edit the parameter
func newEditingOption(infoText string, slider *editingSlider, defaultValue float64) *fyne.Container {
	data := binding.BindFloat(&defaultValue)
	text := lbl(infoText)
	value := lblD(binding.FloatToStringWithFormat(data, "%.0f"))
	slider.Bind(data)
	slider.Step = 1

	return vbox(
		hbox(
			text,
			space(),
			value,
		),
		slider,
	)
}

// func parseURL(urlStr string) *url.URL {
// 	link, err := url.Parse(urlStr)
// 	if err != nil {
// 		fyne.LogError("Could not parse URL", err)
// 	}
// 	return link
// }

// App represents the whole application with all its windows, widgets and functions
type App struct {
	app     fyne.App
	mainWin fyne.Window

	img        Img
	mainModKey fyne.KeyModifier
	focus      bool
	lastOpened []string

	image *canvas.Image

	sliderBrightness    *editingSlider
	sliderContrast      *editingSlider
	sliderHue           *editingSlider
	sliderSaturation    *editingSlider
	sliderColorBalanceR *editingSlider
	sliderColorBalanceG *editingSlider
	sliderColorBalanceB *editingSlider
	sliderSepia         *editingSlider
	sliderBlur          *editingSlider
	resetBtn            *widget.Button

	split       *container.Split
	widthLabel  *widget.Label
	heightLabel *widget.Label
	imgSize     *widget.Label
	imgLastMod  *widget.Label

	statusBar    *fyne.Container
	leftArrow    *widget.Button
	rightArrow   *widget.Button
	deleteBtn    *widget.Button
	renameBtn    *widget.Button
	zoomIn       *widget.Button
	zoomOut      *widget.Button
	zoomLabel    *widget.Label
	resetZoomBtn *widget.Button

	fullscreenWin fyne.Window
}

func reverseArray(arr []string) []string {
	for i := 0; i < len(arr)/2; i++ {
		j := len(arr) - i - 1
		arr[i], arr[j] = arr[j], arr[i]
	}
	return arr
}

func (a *App) init() {
	a.img = Img{}
	a.img.init()

	// theme
	// switch a.app.Preferences().StringWithFallback("Theme", "Dark") {
	// case "Light":
	// 	a.app.Settings().SetTheme(theme.LightTheme())
	// case "Dark":
	// 	a.app.Settings().SetTheme(theme.DarkTheme())
	// }

	// show/hide statusbar
	if !a.app.Preferences().BoolWithFallback("statusBarVisible", true) {
		a.statusBar.Hide()
	}
}

func UI(a fyne.App, imgfile string) {
	w := a.NewWindow("Image Viewer-io.github.palexer.image")
	// a.SetIcon(resourceIconPng)
	w.SetIcon(resourceIconPng)
	ui := &App{app: a, mainWin: w}
	ui.init()
	w.SetContent(ui.loadMainUI())
	// if len(os.Args) > 1 {
	// 	file, err := os.Open(os.Args[1])
	// 	if err != nil {
	// 		fmt.Printf("error while opening the file: %v\n", err)
	// 	}
	// 	ui.open(file, true)
	// }
	file, err := os.Open(imgfile)
	if err != nil {
		fmt.Printf("error while opening the file: %v\n", err)
	}

	if file != nil {
		ui.open(file, true)
	}
	w.Resize(fyne.NewSize(1000, 700))
	w.Show()
	// w.ShowAndRun()
}
