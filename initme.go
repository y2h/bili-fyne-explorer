package main

import (
	"path/filepath"
	"slices"

	"fyne.io/fyne/v2"
	bd "fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/storage"
	"fyne.io/fyne/v2/theme"
	. "fyne.io/fyne/v2/widget"
	"gitee.com/y2h/fyneExplorer/me"
)

func (f *fileDialog) initme() {
	me.Win = f.w
	f.m = me.NewMe()
	f.menuRight()

	me.BtnPaste = NewButtonWithIcon("", theme.ContentPasteIcon(), func() {
		src := filepath.Clean(me.PathToPasted)
		// dest := filepath.Join(f.dir.Path(), filepath.Base(src))
		// if src == dest {
		// 	suffix := fmt.Sprint(time.Now().UnixMilli())
		// 	dest = filepath.Join(f.dir.Path(),
		// 		gfile.Name(src)+"_"+suffix+gfile.Ext(src))
		// }
		// _, err := Copy(src, dest)

		err := CopyDir(src, f.dir.Path())
		if err != nil {
			me.Msg("粘贴失败：" + err.Error())
			return
		}
		me.Msg("成功粘贴文件：" + src)
		f.refreshDir(f.dir)
	})
	me.BtnPaste.Disable()

	me.BtnDelFavorite = NewButtonWithIcon("", theme.DeleteIcon(), func() {
		if f.dir == nil || !slices.Contains(me.FavoritesAdded, f.dir.Path()) {
			me.Msg("未选择收藏夹项目")
			return
		}
		dialog.ShowConfirm("删除收藏夹项目", "您确定删除吗？"+f.dir.Path(), func(b bool) {
			if b {
				f.favorites = slices.DeleteFunc(f.favorites, func(fi favoriteItem) bool {
					return fi.loc.Path() == f.dir.Path()
				})
				me.FavoritesAdded = slices.DeleteFunc(me.FavoritesAdded, func(fi string) bool {
					return fi == f.dir.Path()
				})
				f.favoritesList.Refresh()
				me.Msg("成功删除收藏夹:" + f.dir.Path())
			}
		}, f.w)
	})
	me.BdFilterType.AddListener(bd.NewDataListener(func() {
		if f.dir != nil {
			switch me.FilterType {
			case me.OptsFilter[3]:
				f.filter = storage.NewExtensionFileFilter(me.ExtImage)
			case me.OptsFilter[4]:
				f.filter = storage.NewExtensionFileFilter(me.ExtAudio)
			case me.OptsFilter[5]:
				f.filter = storage.NewExtensionFileFilter(me.ExtVideo)
			case me.OptsFilter[6]:
				f.filter = storage.NewExtensionFileFilter(me.ExtZip)
			}
			f.refreshDir(f.dir)
		}
	}))
	me.RefreshDir = func() {
		if f.dir != nil {
			f.refreshDir(f.dir)
		}
	}
	me.BdRefreshDir.AddListener(bd.NewDataListener(func() {
		i := slices.Index(me.OptsSort, me.FnSort)
		if i > -1 {
			me.SortFn = me.FirstFn[i]
		}
		if f.dir != nil {
			f.refreshDir(f.dir)
		}
	}))

	me.PrevView = func(istype func(string) bool, fn func(fyne.URI)) {
		if f.selectedID > 0 {
			for i := f.selectedID - 1; i > -1; i-- {
				// if me.IsImage(f.data[i].Path()) {
				if istype(f.data[i].Path()) {
					f.files.Select(i)
					if fn != nil {
						fn(f.selected)
					}
					// me.ViewImage(f.data[i].Path())
					break
				}
			}
		}
	}
	me.NextView = func(istype func(string) bool, fn func(fyne.URI)) {
		if f.selectedID < len(f.data) {
			for i := f.selectedID + 1; i < len(f.data); i++ {
				// if me.IsImage(f.data[i].Path()) {
				if istype(f.data[i].Path()) {
					f.files.Select(i)
					if fn != nil {
						fn(f.selected)
					}
					// me.ViewImage(f.data[i].Path())
					break
				}
			}
		}
	}
}
