package me

import (
	_ "embed"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

//go:embed icons/delete.svg
var DeleteI []byte

//go:embed icons/rename.svg
var RenameI []byte

//go:embed icons/view.svg
var Viewi []byte
var (
	ResDelete = fyne.NewStaticResource("delete", DeleteI)
	ResRename = fyne.NewStaticResource("name", RenameI)
	ResView   = fyne.NewStaticResource("view", Viewi)
)

type ClickIcon struct {
	widget.Icon
	fn func()
}

func NewClickIcon(res fyne.Resource,fn func()) *ClickIcon {
	icon := &ClickIcon{}
	icon.ExtendBaseWidget(icon)
	icon.Resource = res
	icon.fn=fn
	return icon
}
func newClickIcon1(name string, content []byte) *ClickIcon {
	icon := &ClickIcon{}
	icon.ExtendBaseWidget(icon)
	icon.Resource = fyne.NewStaticResource(name, content)
	return icon
}
func (i *ClickIcon) Tapped(*fyne.PointEvent) {
	if i.fn != nil {
		i.fn()
	}
}
