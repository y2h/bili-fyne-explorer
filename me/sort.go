package me

import (
	"path/filepath"
	"slices"
	"strings"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

// 在 Select 的选择事件中，通过 fn 调用 fileDialog 的相关方法，如 refreshDir 方法
func (m *Me) SelSort(fn func()) *widget.Select {

	firstFn := []func(a, b fyne.URI) int{
		DirFirst, FileFirst,
		MeFirst(ExtImage...),
		MeFirst(ExtAudio...),
		MeFirst(ExtVideo...),
		MeFirst(ExtZip...),
	}
	return widget.NewSelect(OptsSort, func(s string) {
		i := slices.Index(OptsSort, s)
		if i > -1 {
			m.SortFn = firstFn[i]
		}
		// switch s {
		// case "文件夹在前":
		// 	m.SortType = 1
		// 	m.SortFn = DirFirst
		// case "文件在前":
		// 	m.SortType = 2
		// 	m.SortFn = FileFirst
		// case "图片在前":
		// 	m.SortFn = MeFirst(".jpg", ".jpeg", ".png", ".bmp")
		// case "视频在前":
		// 	m.SortFn = MeFirst(".mp4", ".avi")
		// case "压缩包在前":
		// 	m.SortFn = MeFirst(".zip", ".rar", ".7z", ".gz")
		// 	// m.SortFn = ZipFirst
		// case "Mp3First":
		// 	m.SortFn = MeFirst(".mp3", ".wav")
		// }
		fn()
	})
}

// 在 fileDialog 的 refreshDir 方法中调用此方法，对子文件(夹)进行排序
func Sort(icons []fyne.URI, fn func(a, b fyne.URI) int) {
	slices.SortStableFunc(icons, fn)
}

func DirFirst(a, b fyne.URI) int {
	return -FileFirst(a, b)
}

func FileFirst(a, b fyne.URI) int {
	if IsDir(a) && !IsDir(b) {
		return 1
	}
	if !IsDir(a) && IsDir(b) {
		return -1
	}

	return SortName(a, b)
}

func IsDir(a fyne.URI) bool {
	_, isDir := a.(fyne.ListableURI)
	return isDir
}

func FileType(a fyne.URI, exts ...string) bool {
	return slices.Contains(exts, strings.ToLower(filepath.Ext(a.Path())))
}

func SortName(a, b fyne.URI) int {
	if n := strings.Compare(a.Name(), b.Name()); n != 0 {
		return n
	} else {
		return 1
	}
}
func MeFirst(exts ...string) func(a, b fyne.URI) int {
	return func(a, b fyne.URI) int {
		return UriFirst(a, b, exts...)
	}
}

func UriFirst(a, b fyne.URI, exts ...string) int {
	isa := FileType(a, exts...)
	isb := FileType(b, exts...)
	if isa && !isb {
		return -1
	}
	if !isa && isb {
		return 1
	}
	return FileFirst(a, b)
}
