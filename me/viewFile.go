package me

import "fyne.io/fyne/v2"

func ViewFile(fpath string, dir fyne.ListableURI) {
	// if IsText(fpath) {
	// 	ViewText(fpath, dir)
	// me.AsyncCall(5, func() {
	// 	me.ViewText(f.selected.Path())
	// }, func() {
	// 	me.Msg("读取文件超时,请使用系统默认应用程序打开")
	// })
	// }

	switch {
	case IsText(fpath):
		// ViewText3(fpath)
		ViewText(fpath, dir)
	case IsImage(fpath):
		ViewImage(fpath)
	case IsGif(fpath):
		ViewGif(fpath)
	}
}
