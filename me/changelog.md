## 51在图片中隐藏信息

fx "fyne.io/x/fyne/widget"
// 显示gif文件
Gif *fx.AnimatedGif
Gif = &fx.AnimatedGif{}
```go
// cyy_viewGif.go
Gif.Stop()
Gif, err = fx.NewAnimatedGif(gifuri)
if err != nil {
    LblMsg.SetText(err.Error())
    return
}
Gif.Start()

// viewFile.go
case IsGif(fpath):
		ViewGif(fpath)

// util.go
func IsGif(fpath string) bool {
	if !gfile.DefaultPermOpen.IsRegular() {
		return false
	}

	return strings.ToLower(filepath.Ext(fpath)) == ".gif"
}
```
重构 NextImage 和 PrevImage 函数，让其更通用
NextView, PrevView      func(func(string) bool, func(fyne.URI))

## 50在图片中隐藏信息

"github.com/auyer/steganography"

## 49整合开源项目imageviewer

https://github.com/Palexer/image-viewer
修改 main.go 中的启动相关代码

注释掉相关代码以支持中文
```go
// main.go init()
// theme
// switch a.app.Preferences().StringWithFallback("Theme", "Dark") {
// case "Light":
// 	a.app.Settings().SetTheme(theme.LightTheme())
// case "Dark":
// 	a.app.Settings().SetTheme(theme.DarkTheme())
// }

// ui.go
// fyne.NewMenuItem("Preferences", a.loadSettingsUI) 
```


## 48提取图片文字Ocr

windows安装 tesseract
https://digi.bib.uni-mannheim.de/tesseract/
https://digi.bib.uni-mannheim.de/tesseract/tesseract-ocr-w64-setup-5.3.3.20231005.exe
https://digi.bib.uni-mannheim.de/tesseract/tessdata_fast/chi_sim.traineddata

## 47查看上一张、下一张图片

在 me.go 中定义2个函数
在 me.go 中定义2个函数
NextView, PrevView    func()

```go
// cyy_viewImg.go
btnPrev := NewButtonWithIcon("", theme.MediaSkipPreviousIcon(), func() {
    PrevView()
})
btnNext := NewButtonWithIcon("", theme.MediaSkipNextIcon(), func() {
    NextView()
})

// initme.go
me.PrevView = func() {
    if f.selectedID > 0 {
        for i := f.selectedID - 1; i > -1; i-- {
            if me.IsImage(f.data[i].Path()) {
                f.files.Select(i)
                me.ViewImage(f.data[i].Path())
                break
            }
        }
    }
}
me.NextView = func() {
    if f.selectedID < len(f.data) {
        for i := f.selectedID + 1; i < len(f.data); i++ {
            if me.IsImage(f.data[i].Path()) {
                f.files.Select(i)
                me.ViewImage(f.data[i].Path())
                break
            }
        }
    }
}

```

## 46分割图片，绘制分割线

```go
// 红色的竖线
line0 := imaging.New(5, mh, Red)
img0.Image = imaging.Paste(img0.Image, line0, image.Pt(w1, 0))
img0.Refresh()
```


## 45分割图片

演示该库的相关用法
github.com/disintegration/imaging

定义全局变量用来表示当前选择文件 
SelectedPath             string

分割后的文件保存在当前目录下，
在 me 包下定义刷新函数，分割后显示刷新文件
 RefreshDir     func()

```go
// initme.go
me.RefreshDir = func() {
    if f.dir != nil {
        f.refreshDir(f.dir)
    }
}
```
## 44查看大图片文件

大图片
https://www.eso.org/public/images/eso1242a/

设置图片显示模式
img0.FillMode=canvas.ImageFillContain

根据图片大小，大于400 X 400 的文件缩小显示
github.com/disintegration/imaging
添加查看原图按钮，加入超时特性，

```go
// cyy_viewImg.go
btnSrc := NewButton("原图", func() {
    AsyncCall1(5, func() {
        img0.File = imgfile
        img0.Refresh()
    }, func() {
        TxtEntry.SetText("读取文件超时,请使用系统默认应用程序打开")
    })
})
```


分析 image.go 源码
添加 .svg 显示

## 43查看图片文件

新建文件 cyy_viewImg.go
使用 canvas.Image 显示图片文件
首先判断是否是图片文件

```go
// util.go
func IsImage(imgfile string) bool {
    if !gfile.DefaultPermOpen.IsRegular() {
		return false
	}
	// if slices.Contains(append(ExtImage,".svg"), gfile.Ext(imagefile)) {
	if slices.Contains(append(ExtImage), gfile.Ext(imagefile)) {
		return true
	}

	return false
}

// cyy_viewImg.go
func ViewImage(imgfile string) {
	img0 := canvas.NewImageFromFile(imgfile)
	img0.Refresh()
	SetObjs(Right, img0)
}


// viewFile.go

switch {
case IsText(fpath):
    ViewText(fpath, dir)
case IsImage(fpath):
    ViewImage(fpath)
}

```

## 42实现小图标点击事件和双击查看文件

将 wclickIcon.go 文件移动到 me 包下。
在 me 包下新建 viewFile.go 文件。

```go 

func (i *fileDialogItem) DoubleTapped(*fyne.PointEvent) {
	// if listable, err := storage.CanList(i.location); err == nil && listable {
	if i.dir {
		i.picker.setLocation(i.location)
		return
	}
// 双击文件的时候查看文件
	me.ViewFile(i.location.Path(), i.picker.dir)
	// i.Tapped(nil)
}

// fileitem.go
func (i *fileDialogItem) Select() {
	i.picker.files.Select(i.id)
}

// CreateRenderer() 
i.iconDelete = me.NewClickIcon(me.ResDelete, func() {
    i.Select()
    i.picker.Delete()
})
i.iconRename = me.NewClickIcon(me.ResRename, func() {
    i.Select()
    i.picker.Rename()
})
i.iconView = me.NewClickIcon(me.ResView, func() {
    me.ViewFile(i.location.Path(), i.picker.dir)
})
```

## 41保存文本框内容到文件

在 me.go 中定义 Win fyne.Window ，在 initme.go 中初始化 me.Win = f.w ，用于显示对话框

```go
btnSave := widget.NewButton("Save", func() {
    dl := dialog.NewFileSave(func(uc fyne.URIWriteCloser, err error) {
        if err != nil {
            dialog.ShowError(err, Win)
            return
        }
        if uc == nil {
            // log.Println("Cancelled")
            return
        }
        gfile.PutContents(uc.URI().Path(), TxtEntry.Text)
    }, Win)
    dl.SetLocation(u)
    dl.Show()
})

```

## 40按行数分页查看文本文件内容

显示文件行数
github.com/bitfield/script

```go
lns, _ := script.File(txtpath).CountLines()
CvsCharSet.Text=strconv.Itoa(lns)

rs, _ := script.File(txtpath).Slice()
bp := NewPage(len(rs), 300)

setContent := func(l, r int) {
    if bp != nil {
        ss := strings.Join(rs[l:r], "\n")
        charset, _ := detector.DetectBest([]byte(ss))
        if charset.Language == "zh" {
            ss, _, _ = transform.String(simplifiedchinese.GBK.NewDecoder(), ss)
        }
        TxtEntry.SetText(ss)
        lblPage.Text = fmt.Sprintf("P%3d/%3d", bp.CurPage(), bp.TotalPage())
        lblPage.Refresh()
    }
}

```

## 39判断文件编码，解决中文乱码问题

```go
// cyy_viewText.go
// github.com/saintfish/chardet
detector := chardet.NewTextDetector()
charset, err := detector.DetectBest(bs)
if err != nil {
    panic(err)
}

// println(charset.Charset)
// println(charset.Language)
if charset.Language=="zh"{
    bs=GbkToUtf8(bs)
}

// util.go
// "golang.org/x/text/encoding/simplifiedchinese"
// 	"golang.org/x/text/transform"
func GbkToUtf8(s []byte) []byte {
	bs, _, err := transform.Bytes(simplifiedchinese.GBK.NewDecoder(), s)
	if err != nil {
		return s
	}
	return bs
}

```
用canvas.Text显示文件编码和大小gfile.SizeFormat(filepath)
CvsCharSet *canvas.Text
CvsFileSize  *canvas.Text


## 38按字节数分页查看文本文件内容

page.go

``` golnag
bp := NewPage(len(bytes.Runes(bs)), 4000)

lblPage := canvas.NewText("", Red)

setContent := func(l, r int) {
    if bp != nil {
        Content.SetText(string(bs[l:r]))
        lblPage.Text = fmt.Sprintf("P%d/%d", bp.CurPage(), bp.TotalPage())
        lblPage.Refresh()
    }
}

fbtns := wdg.FourBtns(func() {
    setContent(bp.First())
}, func() {
    setContent(bp.Prev())
}, func() {
    setContent(bp.Next())
}, func() {
    setContent(bp.Last())
})
btnAll := widget.NewButton("All", func() {
    setContent(0, len(bs))
})
pageToGo := 1
btnGo := btn("Go", func() {
    bp.GetPage(pageToGo)
    setContent(bp.CurIndexs())
})
eni := widget.NewEntryWithData(binding.IntToString(binding.BindInt(&pageToGo)))
eni.OnSubmitted = func(s string) {
    btnGo.OnTapped()
}

SetObjs(Right, border(container.NewPadded(hbox(BtnSave, BtnClear,
    fbtns, btnAll, lblPage, eni, btnGo)),
    MsgT(fp), nil, nil, Content))

```


## 37查看当前选定文本文件内容超时取消

超时处理，
代码执行时间：
```go
t1 := time.Now()
fn()
println(time.Now().Sub(t1).Milliseconds())
```

```go
// me.go
Ctx = context.Background()

// util.go
// me.AsyncCall(5, func() {
    // readfile()
//     }, func() {
// me.Msg("读取文件超时,请使用系统默认应用程序打开")
// })
func AsyncCall(outi time.Duration, fn, fn1 func()) {
	ctx, cancel := context.WithTimeout(Ctx, time.Duration(time.Second*outi))
	defer cancel()
	done := make(chan struct{}, 1)
	go func() {
		if fn != nil {
			fn()
		}
		done <- struct{}{}
	}()

	select {
	case <-done:
		return
	case <-ctx.Done():
		if fn1 != nil {
			fn1()
		}
	}
}

func AsyncCall1(outi time.Duration, fn, fn1 func()) {
	done := make(chan struct{}, 1)
	go func() {
		if fn != nil {
			fn()
		}
		done <- struct{}{}
	}()

	select {
	case <-done:
		// fmt.Println("call successfully!!!")
		return
	case <-time.After(time.Duration(outi * time.Second)):
		// fmt.Println("timeout!!!")
		if fn1 != nil {
			fn1()
		}
		return
	}
}

```
1. 用默认应用程序打开


2. 分页
    - 对[]rune进行分页
        bytes.Runes()
    - 对[]linestring 进行分页
Widget.Entry 不适用显示大量文本



## 36查看当前选定文本文件内容

1. 增加查看文件的右键菜单

cyy_rightMenu.go

``` glang
fyne.NewMenuItem("View", func() {
    if me.IsText(f.selected.Path()) {

    }
}),
```

2. 判断当前选定项的文件类型
判断文件类型的包：
 - github.com/gabriel-vasile/mimetype
 - github.com/wailsapp/mimetype
 - github.com/h2non/go-is-svg
 - github.com/h2non/filetype

Fast, dependency-free Go package to infer binary file types based on the magic numbers header signature

或者用后缀名判断

```go

ExtText=[]string{"txt","go","mod","sum","md","json","c","cpp","h","hpp","java"}

func IsText(textfile string) bool {
	if !gfile.DefaultPermOpen.IsRegular() {
		return false
	}
	if slices.Contains(ExtText, gfile.ExtName(textfile)) {
		return true
	}

	mtype, err := mimetype.DetectFile(textfile)
	if err != nil {
		return false
	}
	if strings.Split(mtype.String(), "/")[0] == "text" {
		return true
	}
	return false
}

```

3. 读取文本文件内容，转换成 string 类型 

```go 
bs, err := os.ReadFile(txtFilePath)
```

4. 用 Entry 控件显示文本文件内容

CtnCenter HSplit
在窗体右侧放置 border 容器
```go
TxtEntry *widget.Entry
TxtEntry.SetText(string(bs))

```


## 35粘贴选定的文件或选定目录下的所有文件(非递归)，如有同名文件则覆盖

```go
me.BtnPaste = widget.NewButtonWithIcon("", theme.ContentPasteIcon(), func() {
    src := filepath.Clean(me.PathToPasted)
    err := CopyDir(src, f.dir.Path())
    if err != nil {
        me.Msg("粘贴失败：" + err.Error())
        return
    }
    me.Msg("成功粘贴文件：" + src)
    f.refreshDir(f.dir)
})

```

## 34粘贴当前选定项

新建文件 cyy_copy.go
在文件me.go增加变量PathToPasted string保存需要粘贴的路径, 
再增加粘贴按钮BtnPaste *widget.Button，在文件initme.go中初始化，
默认为不可点击，当执行了复制路径操作后，设置为可用状态，
粘贴到用户选择的目标路径f.dir。

```go
// initme.go
me.BtnPaste = widget.NewButtonWithIcon("", theme.ContentPasteIcon(), func() {
    src := filepath.Clean(me.PathToPasted)
    dest := filepath.Join(f.dir.Path(), filepath.Base(src))
    if src == dest {
        suffix := fmt.Sprint(time.Now().UnixMilli())
        dest = filepath.Join(f.dir.Path(),
            gfile.Name(src)+"_"+suffix+gfile.Ext(src))
    }
    _, err := Copy(src, dest)
    if err != nil {
        me.Msg("粘贴失败：" + err.Error())
        return
    }
    me.Msg("成功粘贴文件：" + src)
    f.refreshDir(f.dir)
})
me.BtnPaste.Disable()

// https://github.com/mactsouk/opensource.com/blob/master/cp1.go
func  (f *fileDialog)Copy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

// cyy_rightMenu.go CpPath() 
me.PathToPasted = f.selected.Path()
me.BtnPaste.Enable()
```

```go 
src := me.PathToPasted
dest := f.dir.Path()
err := CopyDir(src, dest)
if err != nil {
    me.Msg("复制失败：" + err.Error())
    return
}
me.Msg("成功粘贴文件：" + src)
f.refreshDir(f.dir)

```

## 33复制当前选定项路径到剪贴板

```go
// 增加菜单项
fyne.NewMenuItem("复制路径", func() {
    f.CpPath()
}),


func (f *fileDialog) CpPath() {
	if f.selected == nil || f.selected.Path() == "" {
		me.Msg("没有选择项目")
		return
	}
	f.w.Clipboard().SetContent(f.selected.Path())
	me.Msg("路径已复制到剪贴板:" + f.selected.Path())
}

```

## 32用系统自带程序打开选定项

```go
// 调用
f.OpenWithDefaultApp(func(err error) {
    me.Msg(err.Error())
})

func (f *fileDialog) OpenWithDefaultApp(errfn func(err error)) {
	path := f.selected.Path()
	if f.selected == nil || path == "" {
		me.Msg("请选择需要打开的项")
		return
	}
	if gfile.Exists(path) {
		var err error
		switch runtime.GOOS {
		case "linux":
			err = exec.Command("xdg-open", path).Start()
		case "windows":
			err = exec.Command("rundll32", "url.dll,FileProtocolHandler", path).Start()
		case "darwin":
			err = exec.Command("open", path).Start()
		default:
			err = fmt.Errorf("unsupported platform")
		}
		if err != nil && errfn != nil {
			errfn(err)
		}
	}
}

// 只能打开部分文件，例如图片png、文本文件、部分视频文件等，对office、pdf等文件则无能为力
func (f *fileDialog) OpenWithDefaultApp1(errfn func(err error)) {
    path := f.selected.Path()
	if f.selected == nil || path == "" {
		me.Msg("请选择需要打开的项")
		return
	}
    fpath, err := url.Parse(f.selected.Path())
    if err != nil {
        me.Msg(err.Error())
        return
    }
    err = fyne.CurrentApp().OpenURL(fpath)
    if err != nil {
        me.Msg(err.Error())
    }
}

```

## 31重命名选定项

```go
func (f *fileDialog) Rename() {
    if f.selected == nil {
        return
    }
    newNameEntry := widget.NewEntry()
    newNameEntry.FocusGained()
    newNameEntry.SetText(f.selected.Name())
    dialog.ShowForm("Rename", "Rename", "Cancel", []*widget.FormItem{
        {
            Text:   "Old Name",
            Widget: widget.NewLabel(f.selected.Path()),
        },
        {
            Text:   "New Name",
            Widget: newNameEntry,
        },
    }, func(s bool) {
        if !s || newNameEntry.Text == "" {
            return
        }

        newNamePath := filepath.Join(f.dir.Path(), newNameEntry.Text)
        err := os.Rename(f.selected.Path(), newNamePath)
        if err != nil {
            dialog.ShowError(errors.New("Rename err : "+err.Error()), f.w)
        }
        f.refreshDir(f.dir)
        me.Msg("成功重命名")
    }, f.w)
}

```


## 30删除选定项

随着需求的增加，右键菜单会变得复杂，于是放到单独的文件cyy_rightMenu.go中，便于后期维护管理。

在<mark>me</mark>包中
定义右键菜单 <mark>MenuRight *widget.PopUpMenu</mark>，
在<mark>cyy_rightMenu.go</mark>增加<mark>func (f *fileDialog) menuRight()</mark> 并在<mark>initme.go</mark>中调用该方法对<mark>MenuRight</mark>进行初始化。

删除代码如下：
```go 
if f.selected != nil {
    dialog.ShowConfirm("Delete file",
        "Are you sure to delete \n\n[ "+f.selected.Path()+" ]", func(b bool) {
            if b {
                f.files.Unselect(f.selectedID)
                storage.Delete(f.selected)
                f.refreshDir(f.dir)
                // 删除后自动选择下一个
                if f.selectedID < len(f.data)-1 {
                    f.files.Select(f.selectedID)
                } else {
                    f.files.Select(f.selectedID - 1)
                }
            }
        }, f.w)
}

```

## 29删除用户添加的收藏夹

1. 通过分析 <mark>file.go</mark> 中的<mark>f.favoritesList.OnSelected </mark>获取用户选择的收藏夹项 <mark>f.dir</mark>

2. 在<mark>me</mark>包中定义删除按钮 <mark>BtnDelFavorite</mark>，放在左侧列表下方:

<mark>file.go</mark>
```go
container.NewBorder(nil,me.BtnDelFavorite,nil,nil,
		f.favoritesList),
```

3. 在<mark>file.go</mark>的<mark> makeUI()</mark>方法中对 <mark>BtnDelFavorite</mark>进行初始化
删除逻辑是：当前选择的项不为nil,且包含在 <u>**FavoritesAdded**</u> 中，调用go1.21 slices包中的相关方法,
从 <u>**FavoritesAdded**</u> 和 <u>**favorites**</u> 删除指定项。

```go
me.BtnDelFavorite = widget.NewButtonWithIcon("", theme.DeleteIcon(), func() {
    if f.dir == nil || !slices.Contains(me.FavoritesAdded, f.dir.Path()) {
        me.Msg("未选择收藏夹项目")
        return
    }
    dialog.ShowConfirm("删除收藏夹项目", "您确定删除吗？"+f.dir.Path(), func(b bool) {
        if b {
            f.favorites = slices.DeleteFunc(f.favorites, func(fi favoriteItem) bool {
                return fi.loc.Path() == f.dir.Path()
            })
            me.FavoritesAdded = slices.DeleteFunc(me.FavoritesAdded, func(fi string) bool {
                return fi == f.dir.Path()
            })
            f.favoritesList.Refresh()
            me.Msg("成功删除收藏夹:" + f.dir.Path())
        }
    }, f.w)
})


```


## 28完善添加到收藏夹功能

1. <mark>只有目录才添加。

在**me**包下定义 <u>**CvsMsg**</u> 将其放到窗口右下角来显示操作信息。

<mark>**me.go**

```
var CvsMsg   *canvas.Text

CvsMsg = canvas.NewText("", Red)

func Msg(msg string){
    CvsMsg.Text=msg
    CvsMsg.Refresh()
}

```

<mark> **fileitem.go**

```go
if !me.IsDir(i.location) {
    me.Msg("请选择目录")
    return
}

```

2. <mark>程序启动时加载用户添加的收藏夹列表

因为需要保存用户添加的收藏夹列表，在**me**包下定义 <u>**FavoritesAdded**</u> 来保存该列表。
在 <u>**fileDialog**</u> 的 <u>**makeUI()**</u> 方法开头添加如下代码：

<mark>**file.go**
```go
p := fyne.CurrentApp().Preferences()
me.FavoritesAdded = p.StringList("favorites")
```

<mark>**loadFavorites**方法最后添加:

```go
for _, v := range me.FavoritesAdded {
    f.favorites = append(f.favorites,
        favoriteItem{locName: filepath.Base(v), locIcon: theme.FolderIcon(), loc: storage.NewFileURI(v)})
}

```

3. <mark>将用户添加的收藏夹列表追加到 **favorites** 中，刷新 **favoritesList**

添加、删除的时同时更新 <u>**FavoritesAdded**</u> 和 <u>**favorites**</u>

```go
f.favorites = append(f.favorites, favoriteItem{locName: f.selected.Name(),
    locIcon: theme.FolderIcon(), loc: f.selected})
f.favoritesList.Refresh()

me.FavoritesAdded = append(me.FavoritesAdded, f.selected.Path())

```

4. <mark>程序退出时保存用户添加的收藏夹列表

在 <u>**fileDialog**</u> 的 <u>**makeUI()**</u> 方法开头添加如下代码：

<mark> **file.go**
```go
f.w.SetOnClosed(func() {
    p.SetStringList("favorites", me.FavoritesAdded)
})

```

## 27将用户常用目录添加到收藏夹，已存在的不添加，即不重复添加
在fileDialogItem的右键单击事件中，添加“添加到收藏夹”菜单项：

```go
fyne.NewMenuItem("添加到收藏夹", func() {
    for _, v := range i.picker.favorites {
        if v.loc.Path() == i.location.Path() {
            println("已在收藏夹")
            return
        }
    }
    i.picker.favorites = append(i.picker.favorites, favoriteItem{
        locName: i.name,
        locIcon: theme.FolderIcon(),
        loc:     i.location,
    })
    i.picker.favoritesList.Refresh()
})
```