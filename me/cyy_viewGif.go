package me

import (
	"fyne.io/fyne/v2"
	ct "fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/storage"
	"fyne.io/fyne/v2/theme"
	. "fyne.io/fyne/v2/widget"
	fx "fyne.io/x/fyne/widget"
)

func ViewGif(fpath string) {
	var err error
	LblMsg := NewLabel("")

	btnPrev := NewButtonWithIcon("", theme.MediaSkipPreviousIcon(), func() {
		PrevView(IsGif, func(u fyne.URI) {
			ViewGif(u.Path())
		})
	})
	btnNext := NewButtonWithIcon("", theme.MediaSkipNextIcon(), func() {
		NextView(IsGif, func(u fyne.URI) {
			ViewGif(u.Path())
		})
	})

	Gif.Stop()
	Gif, err = fx.NewAnimatedGif(storage.NewFileURI(fpath))
	if err != nil {
		LblMsg.SetText(err.Error())
		return
	}
	Gif.Start()
	SetObjs(Right, ct.NewBorder(ct.NewHBox(btnPrev, btnNext),
		ct.NewHBox(CvsMsg, LblMsg), nil, nil, Gif))
}
