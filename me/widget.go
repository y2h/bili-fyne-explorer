package me

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

func FourBtns(fn1, fn2, fn3, fn4 func()) *fyne.Container {
	return container.NewHBox(
		widget.NewButtonWithIcon("", theme.MediaSkipPreviousIcon(), fn1),
		widget.NewButtonWithIcon("", theme.NavigateBackIcon(), fn2),
		widget.NewButtonWithIcon("", theme.NavigateNextIcon(), fn3),
		widget.NewButtonWithIcon("", theme.MediaSkipNextIcon(), fn4),
	)
}
