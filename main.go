package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
)

func main() {
	a := app.NewWithID("gitee.com.y2h.fyneExplorer")
	a.Settings().SetTheme(&theme1{})
	w := a.NewWindow("Fyne Explorer")
	fd0 := &FileDialog{parent: w}
	fd := &fileDialog{file: fd0, w: w}
	w.SetContent(fd.makeUI())
	fd.setView(gridView)
	fd.setLocation(fd0.effectiveStartingDir())
	w.Resize(fyne.NewSize(1280, 800))
	// w.SetFullScreen(true)
	w.ShowAndRun()
}
