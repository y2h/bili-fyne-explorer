# biliFyneExplorer

#### 介绍
在go语言GUI框架fyne的对话框控件的基础上开发一个文件资源管理器，在此基础上进行扩展，可查看文本文件、图片、docx 、sqlite数据库等，同时整合了一些之前开发的小工具，如重命名、图片查看编辑等。

与B站视频同步 https://www.bilibili.com/video/BV1Zb4y1T7n9/

最新实现的效果，可有偿获取此源码，可执行文件可在发行版下载试用
<div align=center>
<img src="main.gif"/>
</div>

#### 软件架构
软件架构说明


#### 安装教程

[fyne框架环境搭建](https://www.bilibili.com/read/cv24882154/)

``` bash
git clone https://gitee.com/y2h/bili-fyne-explorer.git
cd bili-fyne-explorer
``` 

1.  编译windows :
``` bash
go build  -ldflags="-s -w -H windowsgui"
```
2.  编译linux :
``` bash
go build  -ldflags="-s -w"
```

#### 使用说明

1.  运行
``` bash
go mod tidy
go run .
```
2.  xxxx
3.  xxxx

# Donators
如果喜爱, 就请我喝咖啡吧?
<div align=center>
<img src="https://gitee.com/y2h/fyne_gift/raw/master/src/wechat.jpeg" height="320"/>
<img src="https://gitee.com/y2h/fyne_gift/raw/master/src/zfb.png" height="320"/>
</div>

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
